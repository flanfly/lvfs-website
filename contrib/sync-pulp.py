#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods
#
# The machine that runs this script must have the 'requests' module installed,
# for example `yum install -y python-requests`

import os
import argparse
import hashlib
import base64
import sys
import gzip
import posixpath
import fnmatch
from typing import Optional, List
from lxml import etree as ET

import requests


class Pulp:
    def __init__(
        self,
        url: str,
        username: Optional[str],
        token: Optional[str],
        filter_tag: Optional[str],
    ):
        self.url = url
        self.username = username
        self.token = token
        self.filter_tag = filter_tag
        if self.username or self.token:
            self.manifest = "PULP_MANIFEST/vendor"
        else:
            self.manifest = "PULP_MANIFEST"
        self.useragent = os.path.basename(sys.argv[0])
        self.session = requests.Session()

    def _download_file(self, fn: str) -> None:

        url_fn = posixpath.join(self.url, os.path.basename(fn))
        print("Downloading {}…".format(url_fn))
        try:
            headers = {
                "User-Agent": self.useragent,
            }
            rv = self.session.get(url_fn, headers=headers, timeout=5, stream=True)
        except (
            requests.exceptions.ConnectionError,
            requests.exceptions.ReadTimeout,
        ) as e:
            print(str(e))
        else:
            with open(fn, "wb") as f:
                for chunk in rv.raw.stream(0x8000, decode_content=False):
                    f.write(chunk)

    def _verify_file(self, fn: str, csum: str, sz: int) -> bool:

        # prefer SHA-256 checksum
        if csum:
            try:
                with open(fn, "rb") as f:
                    csum_fn = hashlib.sha256(f.read()).hexdigest()
            except FileNotFoundError:
                return False
            if csum_fn != csum:
                print("{} does not match checksum {}".format(fn, csum))
                return False

        # fallback to size
        if sz:
            try:
                sz_fn = os.path.getsize(fn)
            except FileNotFoundError:
                return False
            if sz_fn != sz:
                print("{} does not match size {}: {}".format(fn, sz, sz_fn))
                return False

        # mirror is up to date
        return True

    def sync(self, path: str) -> int:

        # check dir exists
        if not os.path.exists(path):
            print("{} does not exist".format(path))
            return 1

        # always download the PULP_MANIFEST
        try:
            print("Downloading {}…".format(self.manifest))
            url_manifest = posixpath.join(self.url, self.manifest)
            headers = {
                "User-Agent": self.useragent,
            }
            if self.username or self.token:
                basic_token = base64.b64encode(
                    "{}:{}".format(self.username or "", self.token or "").encode()
                ).decode()
                headers["Authorization"] = "Basic {}".format(basic_token)
            rv = self.session.get(url_manifest, headers=headers, timeout=5)
        except (
            requests.exceptions.ConnectionError,
            requests.exceptions.ReadTimeout,
        ) as e:
            print(str(e))
            return 1

        # sanity check
        if rv.status_code != 200:
            print(rv.content.decode())
            return 1

        # parse into lines
        fns_download = []
        metadata_fn = None
        for line in rv.content.decode().split("\n"):
            try:
                fn, csum, sz = line.rsplit(",", 2)
            except ValueError:
                continue
            if not self._verify_file(os.path.join(path, fn), csum, int(sz)):
                fns_download.append(fn)

            # we need this for filtering
            if not metadata_fn and fnmatch.fnmatch(fn, "firmware*.xml.gz"):
                metadata_fn = fn

        # if we specify a tag then only download archives and screenshots that match, otherwise just download everything
        fns_download_filtered: List[str] = []
        if not self.filter_tag:
            fns_download_filtered = fns_download

        else:
            # ensure metadata is downloaded first so we can filter
            if not metadata_fn:
                print("no metadata in pulp filenames")
                return 1
            if metadata_fn in fns_download:
                self._download_file(os.path.join(path, metadata_fn))

            # always add these extensions
            for fn in fns_download:
                if fn == metadata_fn:
                    continue
                if os.path.splitext(fn)[1] in [".gz", ".jcat", ".asc"]:
                    fns_download_filtered.append(fn)

            # parse metadata
            with gzip.open(os.path.join(path, metadata_fn), mode="rb") as f:
                xml_parser = ET.XMLParser()
                components = ET.fromstring(f.read(), xml_parser).xpath(
                    "/components/component"
                )

                for component in components:

                    # does the specified tag exist?
                    tags = [tag.text for tag in component.xpath("tags/tag")]
                    if self.filter_tag not in tags:
                        continue

                    # cabinet archive
                    location = component.xpath(
                        "releases/release/artifacts/artifact/location"
                    )[0]
                    fn = os.path.basename(location.text)
                    if fn in fns_download:
                        fns_download_filtered.append(fn)

                    # also add pngs for filtered components

        # download possibly filtered files
        for fn in fns_download_filtered:
            self._download_file(os.path.join(path, fn))

        # success
        return 0


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Sync metadata using PULP_MANIFEST")
    parser.add_argument("url")
    parser.add_argument("dir")
    parser.add_argument(
        "--username", default=None, help="Username for the LVFS instance"
    )
    parser.add_argument(
        "--token", default=None, help="User token for the LVFS instance"
    )
    parser.add_argument(
        "--filter-tag", default=None, help="Only download firmware matching this tag"
    )
    args = parser.parse_args()

    pulp = Pulp(
        url=args.url,
        username=args.username,
        token=args.token,
        filter_tag=args.filter_tag,
    )
    sys.exit(pulp.sync(args.dir))
