#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-self-use

import os
import datetime
from typing import Dict, List

from cabarchive import CabFile
from lvfs.firmware.models import Firmware
from lvfs.pluginloader import (
    PluginBase,
    PluginError,
    PluginSetting,
    PluginSettingBool,
)


def _get_fw_copyright_range(fw: Firmware) -> str:
    dt_min: datetime.datetime = datetime.datetime.utcnow()
    dt_max: datetime.datetime = datetime.datetime.utcnow()
    dts: List[datetime.datetime] = []
    if fw.timestamp:
        dts.append(fw.timestamp.replace(tzinfo=None))
    if fw.signed_timestamp:
        dts.append(fw.signed_timestamp.replace(tzinfo=None))
    for md in fw.mds:
        if md.release_timestamp:
            dts.append(datetime.datetime.fromtimestamp(md.release_timestamp))
    for dt in dts:
        dt_min = min(dt_min or dt, dt)
        dt_max = max(dt_max or dt, dt)
    if dt_min.year != dt_max.year:
        return "{}-{}".format(dt_min.year, dt_max.year)
    return str(dt_min.year)


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self, "license")
        self.name = "License"
        self.summary = "Add a LICENSE.txt file to the archive"
        self.settings.append(PluginSettingBool(key="license_enable", name="Enabled"))
        self.settings.append(
            PluginSetting(
                key="license_filename", name="Filename", default="LICENSE.txt"
            )
        )

    def archive_finalize(self, cabarchive, fw):

        # add all the known licenses to the archive
        contents: Dict[str, str] = {}
        for md in fw.mds:
            if (
                md.project_license
                and md.project_license.is_approved
                and md.project_license.text
            ):
                tmp = md.project_license.text
                tmp = tmp.replace("$COPYRIGHT_YEAR$", _get_fw_copyright_range(fw))
                tmp = tmp.replace(
                    "$COPYRIGHT_HOLDER$", fw.vendor.legal_name or fw.vendor.display_name
                )
                contents[md.project_license.value] = tmp

        # add to the archive if required
        if not contents:
            return
        filename = self.get_setting("license_filename", required=True)
        cabarchive[filename] = CabFile("\n\n\n".join(contents.values()).encode("utf-8"))
