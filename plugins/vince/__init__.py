#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-self-use,protected-access

import json
from typing import Optional, Dict

import datetime
import dateutil.parser
import requests

from lvfs.firmware.models import Firmware
from lvfs.components.models import Component, ComponentIssue
from lvfs.pluginloader import PluginBase, PluginError, PluginSetting, PluginSettingBool
from lvfs import ploader, db


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self, "vince")
        self.name = "VINCE"
        self.summary = "Add VU# security IDs and auto-set the embargo date"
        self.settings.append(PluginSettingBool(key="vince_enable", name="Enabled"))
        self.settings.append(
            PluginSetting(
                key="vince_apikey",
                name="API Key",
                default="deadbeef",
            )
        )

    def _update_vince_issue(self, issue: ComponentIssue) -> None:

        apikey = self.get_setting("vince_apikey", required=True)
        api = "https://kb.cert.org/vince/comm/api/case/{}/".format(issue.value)
        print("requesting {}…".format(api))
        headers = {"Authorization": "Token {}".format(apikey)}
        r = requests.get(api, headers=headers, stream=True, timeout=10)

        # probably permissions error
        if r.status_code != 200:
            return

        # parse JSON
        try:
            data = json.loads(r.text)
        except json.decoder.JSONDecodeError as e:
            raise PluginError("Failed to query: {}".format(r.text)) from e

        # update date
        try:
            if "publicdate" in data:
                issue.published = dateutil.parser.parse(data["publicdate"]).replace(
                    tzinfo=None
                )
            elif "due_date" in data:
                issue.published = dateutil.parser.parse(data["due_date"]).replace(
                    tzinfo=None
                )
        except dateutil.parser._parser.ParserError as e:  # type: ignore
            raise PluginError("Failed to parse published: {}".format(r.text)) from e

        # update description
        try:
            issue.description = data["summary"]
        except KeyError:
            pass

    def _autoadd_vince_for_cve_text(self, md: Component, r_text: str) -> None:

        # parse JSON
        try:
            data = json.loads(r_text)
        except json.decoder.JSONDecodeError as e:
            raise PluginError("Failed to query: {}".format(r_text)) from e

        # parse VUID
        try:
            vuid = data["note"]["vuid"]
            if vuid.startswith("VU#"):
                vuid = vuid[3:]
        except KeyError as e:
            raise PluginError("Failed to get VUID: {}".format(r_text)) from e

        # does this component have the VU# issue already?
        if vuid in md.issue_values:
            return
        issue = ComponentIssue(kind="vince", value=vuid, user_id=2)
        try:
            issue.description = data["note"]["name"]
        except KeyError:
            pass
        md.issues.append(issue)
        print(
            "added {} to component {} using VINCE".format(
                issue.value_display, md.component_id
            )
        )
        db.session.commit()

    def _update_issue_json(self, issue: ComponentIssue, r_text: str) -> None:

        # already set
        if issue.description and issue.published:
            return

        # parse JSON
        try:
            data = json.loads(r_text)
        except json.decoder.JSONDecodeError as e:
            raise PluginError("Failed to query: {}".format(r_text)) from e
        try:
            description = data["vulnerability"]["description"]
            if not description.startswith("http://"):
                issue.description = description
            published = data["note"]["publicdate"].split("Z")[0]
            issue.published = datetime.datetime.fromisoformat(published)
        except (KeyError, ValueError):
            pass

    def _autoadd_vince_for_cve(self, issue: ComponentIssue) -> None:

        # public search
        api = "https://kb.cert.org/vuls/api/vuls/cve/{}".format(issue.value[4:])
        print("requesting {}…".format(api))
        try:
            r = requests.get(api, stream=True, timeout=10)
        except requests.exceptions.ReadTimeout as e:
            print(str(e))
        else:
            if r.status_code == 200:
                self._autoadd_vince_for_cve_text(issue.md, r.text)
                self._update_issue_json(issue, r.text)

        # private search
        apikey = self.get_setting("vince_apikey", required=True)
        api = "https://kb.cert.org/vince/comm/api/cve/{}/".format(issue.value[4:])
        print("requesting {}…".format(api))
        headers = {"Authorization": "Token {}".format(apikey)}
        try:
            r = requests.get(api, headers=headers, stream=True, timeout=10)
        except requests.exceptions.ReadTimeout as e:
            print(str(e))
        else:
            if r.status_code == 200:
                self._autoadd_vince_for_cve_text(issue.md, r.text)
                self._update_issue_json(issue, r.text)

    def archive_presign(self, fw: Firmware) -> None:

        # add VU# IDs for CVEs
        for md in fw.mds:
            for issue in md.issues:
                if issue.kind in ["cve", "intel-sa"]:
                    self._autoadd_vince_for_cve(issue)

        # update the embargo date
        dt_largest = fw.restricted_ts
        for md in fw.mds:
            for issue in md.issues:
                if issue.kind == "vince":
                    self._update_vince_issue(issue)
                    if not issue.published:
                        continue
                    if not dt_largest or issue.published > dt_largest:
                        dt_largest = issue.published

        # changed
        if fw.restricted_ts != dt_largest:
            print(
                "updated firmware {} restricted_ts from {} to {} using VINCE".format(
                    fw.firmware_id, fw.restricted_ts, dt_largest
                )
            )
            fw.restricted_ts = dt_largest
            db.session.commit()


# run with PYTHONPATH=. ./env/bin/python3 plugins/vince/__init__.py
if __name__ == "__main__":
    plugin = Plugin()
    _fw = Firmware()
    _md = Component()
    _md.issues.append(ComponentIssue(kind="vince", value="999999"))
    _md.issues.append(ComponentIssue(kind="cve", value="CVE-2020-11902"))
    _md.issues.append(ComponentIssue(kind="vince", value="257161"))
    _fw.mds.append(_md)
    plugin.archive_presign(_fw)
    print("restricted_ts", _fw.restricted_ts)
    for _issue in _md.issues:
        print(
            "issue {} = {} [{}]".format(
                _issue.value, _issue.description, _issue.published
            )
        )
