#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-member,too-few-public-methods

import os
import zlib
import tempfile
import glob
from collections import defaultdict
from typing import Dict, Optional, Tuple, List

from fwhunt_scan import UefiAnalyzer, UefiRule, UefiScanner, UefiScannerError

from lvfs import db
from lvfs.pluginloader import (
    PluginBase,
    PluginError,
    PluginSettingBool,
    PluginSettingList,
)
from lvfs.firmware.models import Firmware
from lvfs.tests.models import Test
from lvfs.components.models import Component, ComponentShard, ComponentShardAttribute


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self, "uefi_scanner")
        self.name = "FwHunt"
        self.summary = "Decompile the EFI binary and scan for known issues"
        self.order_after = ["uefi-extract"]
        self.settings.append(
            PluginSettingBool(key="uefi_scanner_enabled", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSettingBool(
                key="uefi_scanner_sandbox",
                name="Sandbox using Bubblewrap",
                default=False,
            )
        )
        self.settings.append(
            PluginSettingList(
                key="uefi_scanner_rules",
                name="Rule locations",
                default=["plugins/uefi_scanner/rules/*"],
            )
        )
        self._uefi_rules: Dict[str, List[UefiRule]] = defaultdict(list)

    def require_test_for_md(self, md):

        # only run for capsule updates
        if not md.protocol:
            return False
        if not md.blob:
            return False
        return md.protocol.value == "org.uefi.capsule"

    def _require_test_for_fw(self, fw: Firmware) -> bool:
        for md in fw.mds:
            if self.require_test_for_md(md):
                return True
        return False

    def ensure_test_for_fw(self, fw):

        # add if not already exists
        if not self._require_test_for_fw(fw):
            return
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    @property
    def _rule_fns(self) -> List[str]:
        fns: List[str] = []
        fn_globs = self.get_setting("uefi_scanner_rules", required=True).split(",")
        for fn_glob in fn_globs:
            fns.extend(glob.glob(fn_glob, recursive=True))
        return fns

    def _run_test_on_shard(self, test: Test, shard: ComponentShard) -> None:

        # only analyze if GUID matches
        if shard.guid not in self._uefi_rules:
            return

        # check if suitable
        if not shard.blob:
            return
        if shard.blob[0:2] != b"MZ":
            return

        # scan files
        rizinhome = None
        if self.get_setting_bool("uefi_scanner_sandbox"):
            rizinhome = os.path.dirname(os.path.realpath(__file__))
        with UefiAnalyzer(blob=shard.blob, rizinhome=rizinhome) as uefi_analyzer:
            try:
                scanner = UefiScanner(uefi_analyzer, self._uefi_rules[shard.guid])
                for result in scanner.results:
                    if not result.res:
                        continue
                    msg = "A potential security issue was detected in {} ({}).".format(
                        shard.name, shard.guid
                    )
                    if result.rule.advisory:
                        msg += " Please see {} for more information.".format(
                            result.rule.advisory
                        )
                    title = "{} (variant: {})".format(
                        result.rule.name or "rule", result.variant_label
                    )
                    test.add_fail(title, msg)
            except UefiScannerError as e:
                msg = ", ".join(
                    [rule.name or "unknown" for rule in self._uefi_rules[shard.guid]]
                )
                test.add_fail("FwHunt", "Cannot run {}: {}".format(msg, str(e)))

    def _ensure_rules(self) -> None:

        # already done
        if self._uefi_rules:
            return
        for rule_fn in self._rule_fns:
            uefi_rule = UefiRule(rule_fn)
            if not uefi_rule.name:
                print("ignoring {} as no name", rule_fn)
                continue
            if not uefi_rule.volume_guids:
                print("ignoring {} as no volume GUIDs", rule_fn)
                continue
            for guid in uefi_rule.volume_guids:
                self._uefi_rules[guid.lower()].append(uefi_rule)

    def run_test_on_md(self, test: Test, md: Component) -> None:

        # load all rules ahead of time
        self._ensure_rules()

        # run analysis on each shard
        for shard in md.shards:
            self._run_test_on_shard(test, shard)


# run with PYTHONPATH=. ./env/bin/python3 plugins/uefi_scanner/__init__.py
if __name__ == "__main__":
    import sys

    plugin = Plugin()
    _test = Test(plugin_id=plugin.id)
    _md = Component()
    try:
        for fn in sys.argv[1:]:
            _shard = ComponentShard(
                name=fn, guid="dcd13040-23d8-41c6-b8f5-22281a0d64e8"
            )
            with open(fn, "rb") as _f:
                blob = _f.read()
                try:
                    _shard.blob = zlib.decompressobj().decompress(blob)
                except zlib.error:
                    _f.seek(0)
                    _shard.blob = blob
            _md.shards.append(_shard)
    except IndexError:
        _shard = ComponentShard()
        _shard.blob = (
            b"MZxxx\x4D\x95\x90\x13\x95\xDA\x27\x42\x93\x28\x72\x82\xC2\x17\xDA\xA8xxx"
        )
        _md.shards.append(_shard)
    plugin.run_test_on_md(_test, _md)
    for attribute in _test.attributes:
        print(attribute)
    for _shard in _md.shards:
        print("\n{}:".format(_shard.name))
        for _attr in _shard.attributes:
            print(_attr.key, _attr.value, _attr.comment or "")
