"""

Revision ID: 8f01e8ee583b
Revises: 7d2c4321b2ab
Create Date: 2020-11-20 12:30:55.340687

"""

# revision identifiers, used by Alembic.
revision = '8f01e8ee583b'
down_revision = '807a2ced5afe'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('components', sa.Column('appstream_type', sa.Text(), nullable=True))
    op.alter_column('components', 'checksum_contents_sha1',
               existing_type=sa.VARCHAR(length=40),
               nullable=True)
    op.alter_column('components', 'checksum_contents_sha256',
               existing_type=sa.VARCHAR(length=64),
               nullable=True)
    op.alter_column('components', 'filename_contents',
               existing_type=sa.TEXT(),
               nullable=True)
    op.alter_column('components', 'version',
               existing_type=sa.TEXT(),
               nullable=True)


def downgrade():
    op.alter_column('components', 'version',
               existing_type=sa.TEXT(),
               nullable=False)
    op.alter_column('components', 'filename_contents',
               existing_type=sa.TEXT(),
               nullable=False)
    op.alter_column('components', 'checksum_contents_sha256',
               existing_type=sa.VARCHAR(length=64),
               nullable=False)
    op.alter_column('components', 'checksum_contents_sha1',
               existing_type=sa.VARCHAR(length=40),
               nullable=False)
    op.drop_column('components', 'appstream_type')
