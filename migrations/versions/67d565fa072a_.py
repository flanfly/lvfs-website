"""

Revision ID: 67d565fa072a
Revises: b93f8a103d05
Create Date: 2022-07-27 10:40:41.150622

"""

# revision identifiers, used by Alembic.
revision = "67d565fa072a"
down_revision = "b93f8a103d05"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column(
        "event_log",
        "addr",
        existing_type=sa.VARCHAR(length=40),
        type_=sa.Text(),
        existing_nullable=False,
    )


def downgrade():
    op.alter_column(
        "event_log",
        "addr",
        existing_type=sa.Text(),
        type_=sa.VARCHAR(length=40),
        existing_nullable=False,
    )
