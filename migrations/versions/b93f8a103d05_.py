"""

Revision ID: b93f8a103d05
Revises: b5f4cb1a8283
Create Date: 2022-05-20 15:01:37.951564

"""

# revision identifiers, used by Alembic.
revision = "b93f8a103d05"
down_revision = "b5f4cb1a8283"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "firmware_actions",
        sa.Column("firmware_action_id", sa.Integer(), nullable=False),
        sa.Column("firmware_id", sa.Integer(), nullable=False),
        sa.Column("remote_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("mtime", sa.DateTime(), nullable=False),
        sa.Column("dtime", sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(
            ["firmware_id"],
            ["firmware.firmware_id"],
        ),
        sa.ForeignKeyConstraint(
            ["remote_id"],
            ["remotes.remote_id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.PrimaryKeyConstraint("firmware_action_id"),
    )
    op.create_index(
        op.f("ix_firmware_actions_firmware_id"),
        "firmware_actions",
        ["firmware_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_firmware_actions_remote_id"),
        "firmware_actions",
        ["remote_id"],
        unique=False,
    )


def downgrade():
    op.drop_index(op.f("ix_firmware_actions_remote_id"), table_name="firmware_actions")
    op.drop_index(
        op.f("ix_firmware_actions_firmware_id"), table_name="firmware_actions"
    )
    op.drop_table("firmware_actions")
