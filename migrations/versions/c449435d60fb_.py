"""

Revision ID: c449435d60fb
Revises: fa7365360f1e
Create Date: 2021-09-22 19:53:50.224294

"""

# revision identifiers, used by Alembic.
revision = "c449435d60fb"
down_revision = "fa7365360f1e"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "user_groups",
        sa.Column("user_group_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("vendor_id", sa.Integer(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.ForeignKeyConstraint(
            ["vendor_id"],
            ["vendors.vendor_id"],
        ),
        sa.PrimaryKeyConstraint("user_group_id"),
    )
    op.create_index(
        op.f("ix_user_groups_user_id"), "user_groups", ["user_id"], unique=False
    )
    op.create_index(
        op.f("ix_user_groups_vendor_id"), "user_groups", ["vendor_id"], unique=False
    )


def downgrade():
    op.drop_index(op.f("ix_user_groups_vendor_id"), table_name="user_groups")
    op.drop_index(op.f("ix_user_groups_user_id"), table_name="user_groups")
    op.drop_table("user_groups")
