"""

Revision ID: dbcc05ad85ad
Revises: 09cf183bb62a
Create Date: 2021-10-24 10:55:03.310069

"""

# revision identifiers, used by Alembic.
revision = "dbcc05ad85ad"
down_revision = "09cf183bb62a"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "component_translations",
        sa.Column("component_description_id", sa.Integer(), nullable=False),
        sa.Column("component_id", sa.Integer(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("value", sa.Text(), nullable=True),
        sa.Column("locale", sa.Text(), nullable=True),
        sa.Column("kind", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["component_id"],
            ["components.component_id"],
        ),
        sa.PrimaryKeyConstraint("component_description_id"),
    )
    op.create_index(
        op.f("ix_component_translations_component_id"),
        "component_translations",
        ["component_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_component_translations_kind"),
        "component_translations",
        ["kind"],
        unique=False,
    )


def downgrade():
    op.drop_index(
        op.f("ix_component_translations_kind"), table_name="component_translations"
    )
    op.drop_index(
        op.f("ix_component_translations_component_id"),
        table_name="component_translations",
    )
    op.drop_table("component_translations")
