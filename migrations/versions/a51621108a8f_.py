"""

Revision ID: a51621108a8f
Revises: d7222878f113
Create Date: 2021-07-29 08:58:15.135551

"""

# revision identifiers, used by Alembic.
revision = "a51621108a8f"
down_revision = "d7222878f113"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("vendors", sa.Column("missing_url", sa.Text(), nullable=True))


def downgrade():
    op.drop_column("vendors", "missing_url")
