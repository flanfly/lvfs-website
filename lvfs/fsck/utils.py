#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison,protected-access,line-too-long

import os
import datetime
from typing import List, Dict

from sqlalchemy import or_

from cabarchive import CabArchive, CorruptionError
from jcat import JcatFile, JcatBlobKind

from lvfs import db, tq, ploader
from lvfs.util import _get_settings
from lvfs.firmware.utils import _async_sign_fw
from lvfs.components.models import (
    Component,
    ComponentIssue,
    ComponentTranslation,
)
from lvfs.firmware.models import Firmware, FirmwareRevision, FirmwareVendor
from lvfs.fsck.models import Fsck
from lvfs.main.models import Event
from lvfs.reports.models import Report, REPORT_ATTR_MAP
from lvfs.users.models import User
from lvfs.users.utils import (
    _user_disable_notify,
    _user_disable_actual,
    _user_add_message_survey,
)
from lvfs.vendors.models import Vendor


def _fsck_update_descriptions(search: str, replace: str) -> None:

    for md in db.session.query(Component):
        for tx in md.release_descriptions:
            if not tx.value:
                continue
            if tx.value.find(search) != -1:
                tx.value = tx.value.replace(search, replace)
    db.session.commit()


@tq.task(max_retries=3, default_retry_delay=5, task_time_limit=600)
def _async_fsck_update_descriptions(search: str, replace: str) -> None:
    _fsck_update_descriptions(search, replace)


@tq.task(max_retries=3, default_retry_delay=5, task_time_limit=600)
def _async_fsck_eventlog_delete(value: str) -> None:

    for evt in (
        db.session.query(Event).filter(Event.message.contains(value)).limit(10000)
    ):
        db.session.delete(evt)
    db.session.commit()


def _fsck_firmware_check_exists(fsck: Fsck, fw: Firmware) -> None:
    """revision no longer exists"""
    for rev in fw.revisions:
        if not os.path.exists(rev.absolute_path):
            fsck.add_fail(
                "EFS",
                "Firmware #{} has missing revision {} {}".format(
                    fw.firmware_id, rev.firmware_revision_id, rev.absolute_path
                ),
            )


def _fsck_firmware_fix_properties(fsck: Fsck, fw: Firmware) -> None:
    """fix up any broken properties"""

    for md in fw.mds:
        if md.release_tag in ["None", ""]:
            fsck.add_fail(
                "Component",
                "Firmware #{} has invalid release tag {}, fixing".format(
                    fw.firmware_id, md.release_tag
                ),
            )
            md.release_tag = None
    db.session.commit()


def _fsck_firmware_add_odm_vendor(fsck: Fsck, fw: Firmware) -> None:
    """add ODM vendor using now-unusd property"""

    if fw.vendor_id == fw._unused_vendor_odm_id:
        return
    vendor = (
        db.session.query(Vendor)
        .filter(Vendor.vendor_id == fw._unused_vendor_odm_id)
        .first()
    )
    if not vendor:
        return
    if vendor in fw.odm_vendors:
        return

    fsck.add_pass(
        "Database::Firmware",
        "Firmware #{} added missing ODM vendor {}".format(
            fw.firmware_id, vendor.group_id
        ),
    )
    fw.odm_vendors_map.append(FirmwareVendor(vendor=vendor, user_id=1))
    db.session.commit()


def _fsck_firmware_consistency(fsck: Fsck, fw: Firmware) -> None:
    """multiple Firmware objects pointing at the same filesystem object"""
    if not fw.revisions:
        return
    for fw2 in (
        db.session.query(Firmware)
        .filter(Firmware.firmware_id != fw.firmware_id)
        .join(FirmwareRevision)
        .filter(
            or_(
                FirmwareRevision.filename == fw.revisions[0].filename,
                Firmware.checksum_upload_sha1 == fw.checksum_upload_sha1,
            )
        )
        .limit(10)
        .all()
    ):
        fsck.add_fail(
            "Database::Firmware",
            "Firmware {} points to {} [SHA1:{}]".format(
                fw2.firmware_id,
                fw2.filename,
                fw2.checksum_upload_sha1,
            ),
        )


def _fsck_firmware_unsigned(fsck: Fsck) -> None:
    """invalidate the signature of firmware signed with an older key"""

    # unset the signed timestamp as required
    settings = _get_settings()
    signed_epoch = int(settings["signed_epoch"])
    for (firmware_id,) in (
        db.session.query(Firmware.firmware_id)
        .filter(Firmware.signed_timestamp != None)
        .order_by(Firmware.firmware_id.asc())
    ):
        if fsck.ended_ts:
            return
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
        # we signed this recently enough to be epoch 1
        if fw.signed_timestamp.replace(tzinfo=None) > datetime.datetime(2020, 3, 5):
            fw.signed_epoch = 1
        # not good enough
        if fw.signed_epoch != signed_epoch:
            fw.signed_timestamp = None
            fsck.add_pass(
                "Database::Firmware",
                "Invaliding signing checksum of {}".format(fw.firmware_id),
            )
    db.session.commit()


def _fsck_firmware_metainfo_nonnull(fsck: Fsck, fw: Firmware, arc: CabArchive) -> None:
    """NUL byte in metainfo file"""

    requires_resign: bool = False

    for fn in arc:
        if not fn.endswith(".xml"):
            continue
        if arc[fn].buf[-1] == 0:
            fsck.add_fail(
                "JCat::Firmware", "NUL found in metainfo #{}".format(fw.firmware_id)
            )
            requires_resign = True
            break

    # invalidate and resign
    if requires_resign:
        fw.signed_timestamp = None
        db.session.commit()
        _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")


def _fsck_firmware_pkcs7_cert_valid(fsck: Fsck, fw: Firmware, arc: CabArchive) -> None:
    """missing cert from PKCS#7 cert"""

    requires_resign: bool = False

    # check each signature has a server CERTIFICATE, not just a signature
    try:
        jcat_file = JcatFile(arc["firmware.jcat"].buf)
    except KeyError:
        fsck.add_fail(
            "JCat::Firmware", "No firmware.jcat in archive #{}".format(fw.firmware_id)
        )
        requires_resign = True
    else:
        for md in fw.mds:

            # files that used to be valid, but are no longer allowed
            if not md.release_installed_size:
                continue

            # check all the things that are supposed be signed in the jcat file
            for fn in [md.filename_contents, md.filename_xml]:
                if not fn:
                    continue
                jcat_item = jcat_file.get_item(fn)
                if jcat_item:
                    # is this big enough to include the certificate?
                    jcat_blob = jcat_item.get_blob_by_kind(JcatBlobKind.PKCS7)
                    if not jcat_blob:
                        fsck.add_fail(
                            "JCat::PKCS7",
                            "No cert for #{}".format(fw.firmware_id),
                        )
                        requires_resign = True
                    elif not jcat_blob.data:
                        fsck.add_fail(
                            "JCat::PKCS7",
                            "No valid cert for #{}".format(fw.firmware_id),
                        )
                        requires_resign = True
                    elif len(jcat_blob.data) < 0x400:
                        fsck.add_fail(
                            "JCat::PKCS7",
                            "Invalid cert for #{}".format(fw.firmware_id),
                        )
                        requires_resign = True
                else:
                    fsck.add_fail(
                        "JCat::Component", "No jcat item for #{}".format(fw.firmware_id)
                    )
                    requires_resign = True

    # invalidate and resign
    if requires_resign:
        fw.signed_timestamp = None
        db.session.commit()
        _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")


def _fsck_firmware_check_archive(fsck: Fsck, fw: Firmware) -> None:
    """load archive from disk"""

    # we've done all these already
    if fw.firmware_id < 7000:
        return

    # load cabarchive
    try:
        arc = CabArchive(fw.blob, flattern=True)
    except CorruptionError:
        fsck.add_fail(
            "EFS::CabArchive", "Cannot load firmware archive #{}".format(fw.firmware_id)
        )
        return

    # cert valid
    _fsck_firmware_metainfo_nonnull(fsck, fw, arc)
    _fsck_firmware_pkcs7_cert_valid(fsck, fw, arc)


def _fsck_firmware_resign(fsck: Fsck, fw: Firmware) -> None:
    """unsigned firmware"""
    if fw.signed_timestamp:
        return
    _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")
    fsck.add_pass("Database::Firmware", "Resigning #{}".format(fw.firmware_id))


def _check_component_issues(fw: Firmware) -> List[ComponentIssue]:
    issues: List[ComponentIssue] = []
    for md in fw.mds:
        for issue in md.issues:
            if issue.kind != "cve":
                continue
            if not issue.description or not issue.published:
                issues.append(issue)
    return issues


def _fsck_firmware_issues(fsck: Fsck, fw: Firmware) -> None:
    """unsigned firmware"""

    # we've done all these already
    if fw.signed_timestamp and fw.signed_timestamp.replace(
        tzinfo=None
    ) > datetime.datetime(2022, 4, 13):
        return

    if not _check_component_issues(fw):
        return
    ploader.archive_presign(fw)
    issues = _check_component_issues(fw)
    if issues:
        fsck.add_fail(
            "Database::ComponentIssue",
            "Cannot fix issues for firmware #{} [{}]".format(
                fw.firmware_id, ",".join([issue.value_display for issue in issues])
            ),
        )
    else:
        fsck.add_pass(
            "Database::ComponentIssue",
            "Fixed issue on #{}".format(fw.firmware_id),
        )


def _fsck_component_keywords(fsck: Fsck, md: Component) -> None:

    for kw in md.keywords:
        if kw.value == "fccl":
            fsck.add_fail(
                "Component::Keywords",
                "Repaired keyword {} firmware #{}".format(kw.value, md.fw.firmware_id),
            )
            kw.value = "fujitsu"


def _fsck_component_translations(fsck: Fsck, md: Component) -> None:

    # both map to actual None for the default...
    for translation in md.translations:
        if translation.locale in ["None", "en_US"]:
            translation.locale = None
            fsck.add_fail(
                "Database::Translations",
                "Repaired translation locale from firmware #{}".format(
                    md.fw.firmware_id
                ),
            )

    # look for dups
    locales: Dict[str, bool] = {}
    to_remove: List[ComponentTranslation] = []
    for translation in md.translations:
        key = "{}:{}".format(translation.kind, translation.locale)
        if key in locales:
            to_remove.append(translation)
        else:
            locales[key] = True

    # actually remove the dups
    for translation in to_remove:
        fsck.add_fail(
            "Database::Translations",
            "Removing duplicate {} translation locale from firmware #{}".format(
                translation.locale, md.fw.firmware_id
            ),
        )
        md.translations.remove(translation)


def _fsck_user_fixup_subgroup(fsck: Fsck, user: User) -> None:
    """fix up user subgroups"""

    # already set
    if user.subgroup:
        return

    open_idx: int = user.display_name.find("(")
    close_idx: int = user.display_name.find(")")
    if open_idx == -1 or open_idx == -1 or open_idx > close_idx:
        return

    user.subgroup = user.display_name[open_idx + 1 : close_idx]
    user.display_name = user.display_name[:open_idx].strip()
    fsck.add_fail(
        "Database::Users",
        "Set subgroup of {} for user {}".format(user.subgroup, user.user_id),
    )
    db.session.commit()


def _fsck_user_fixup_disabled(fsck: Fsck, user: User) -> None:
    """fix up the disabled user details from the event log"""

    if user.auth_type != "disabled":
        return
    if not user.username.startswith("disabled_user"):
        return

    # find what we recorded
    evt = (
        db.session.query(Event)
        .filter(Event.message.startswith("Disabling user {} ".format(user.user_id)))
        .first()
    )
    if not evt:
        return

    # parse the string
    try:
        user.username = evt.message.split(" ")[3]
    except IndexError:
        return
    lbr = evt.message.find("(")
    rbr = evt.message.find(")")
    if lbr != -1 and rbr != -1 and rbr > lbr:
        user.display_name = evt.message[lbr + 1 : rbr]

    # record success
    fsck.add_fail(
        "Database::Users",
        "Restored username {} for user {}".format(user.username, user.user_id),
    )
    db.session.commit()


def _fsck_report_check_deprecated_attrs(fsck: Fsck, report: Report) -> None:
    """fix up report attrs"""

    fixed: List[str] = []
    for key, value in REPORT_ATTR_MAP.items():
        attr = report.get_attribute_by_key(key)
        if attr:
            attr.key = value
            if key not in fixed:
                fixed.append(key)
    if fixed:
        fsck.add_fail(
            "Database::Reports",
            "Fixed up {} keys".format(", ".join(fixed)),
        )
    db.session.commit()


@tq.task(max_retries=3, default_retry_delay=5, task_time_limit=18000)
def _async_fsck(extended: bool) -> None:

    fsck = Fsck()
    fsck.extended = extended
    fsck.container_id = os.environ.get("CONTAINER_ID")
    db.session.add(fsck)
    db.session.commit()

    # force user actions
    _user_disable_notify()
    _user_disable_actual()
    _user_add_message_survey()

    # these need a cache
    _fsck_firmware_unsigned(fsck)

    # fix up users
    for (user_id,) in db.session.query(User.user_id).order_by(User.user_id.asc()):
        user = db.session.query(User).filter(User.user_id == user_id).one()
        _fsck_user_fixup_subgroup(fsck, user)
        _fsck_user_fixup_disabled(fsck, user)

    # fix up reports
    for (report_id,) in db.session.query(Report.report_id).order_by(
        Report.report_id.asc()
    ):
        report = db.session.query(Report).filter(Report.report_id == report_id).one()
        _fsck_report_check_deprecated_attrs(fsck, report)

    # process each task that does not require cache
    for (firmware_id,) in db.session.query(Firmware.firmware_id).order_by(
        Firmware.firmware_id.asc()
    ):
        print("fsck1 on #{}".format(firmware_id))
        if fsck.ended_ts:
            break
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
        _fsck_firmware_check_exists(fsck, fw)
        _fsck_firmware_consistency(fsck, fw)
        _fsck_firmware_add_odm_vendor(fsck, fw)
        _fsck_firmware_fix_properties(fsck, fw)
        _fsck_firmware_resign(fsck, fw)
        if extended:
            _fsck_firmware_check_archive(fsck, fw)
        for md in fw.mds:
            _fsck_component_translations(fsck, md)
            _fsck_component_keywords(fsck, md)
        db.session.commit()

    # lets do the really slow ones last
    for (firmware_id,) in db.session.query(Firmware.firmware_id).order_by(
        Firmware.firmware_id.asc()
    ):
        print("fsck2 on #{}".format(firmware_id))
        if fsck.ended_ts:
            break
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
        if extended:
            _fsck_firmware_issues(fsck, fw)
        db.session.commit()

    # all done
    fsck.ended_ts = datetime.datetime.utcnow()
    db.session.commit()
