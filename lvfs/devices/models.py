#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,protected-access

import datetime

from sqlalchemy import Column, Integer, Text, DateTime, ForeignKey
from sqlalchemy.orm import relationship

from lvfs import db


class Product(db.Model):

    __tablename__ = "products"

    product_id = Column(Integer, primary_key=True)
    appstream_id = Column(Text, nullable=False, index=True)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    mtime = Column(DateTime, nullable=True)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    state = Column(Text, nullable=True, index=True)

    user = relationship("User", foreign_keys=[user_id])

    STATE_ACTIVE = None
    STATE_EOL = "eol"

    @property
    def state_display(self) -> str:
        if self.state == self.STATE_ACTIVE:
            return "active"
        if self.state == self.STATE_EOL:
            return "end-of-life"
        return "unknown"

    def __repr__(self) -> str:
        return "Product object {}".format(self.product_id)
