#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-many-locals

import os
import hashlib

from flask import Blueprint, request, flash, url_for, redirect, render_template, g
from flask_login import login_required

from lvfs import db, csrf, auth

from lvfs.util import (
    _get_client_address,
    _json_error,
    _json_success,
)
from lvfs.firmware.models import FirmwareRevision
from lvfs.firmware.utils import _async_sign_fw
from lvfs.vendors.models import Vendor, VendorAffiliation
from lvfs.util import _get_sanitized_basename, admin_login_required

from .models import Upload
from .utils import _async_upload_firmware, _upload_firmware_raw, _user_can_upload
from .uploadedfile import (
    FileNotSupported,
)

bp_upload = Blueprint("upload", __name__, template_folder="templates")


@bp_upload.route("/list")
@bp_upload.route("/list/limit/<int:limit>")
@login_required
@admin_login_required
def route_list(limit=30):
    uploads = (
        db.session.query(Upload).order_by(Upload.upload_id.desc()).limit(limit).all()
    )
    return render_template("upload-list.html", category="admin", uploads=uploads)


def _robot_upload_sync():

    # uploading for a custom vendor
    if "vendor_id" in request.form:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == int(request.form["vendor_id"]))
            .first()
        )
        if not vendor:
            return _json_error("Specified vendor ID not found")
    else:
        vendor = g.user.vendor

    # create
    try:
        blob = request.files["file"].read()
        fn = "{}-{}".format(
            hashlib.sha256(blob).hexdigest(),
            _get_sanitized_basename(os.path.basename(request.files["file"].filename)),
        )
    except KeyError as e:
        return _json_error("Failed to upload: {}".format(str(e)))
    if os.path.exists(fn):
        return _json_error("The same filename {} already exists".format(fn))

    # continue with form data
    try:
        up = Upload(
            user=g.user,
            addr=_get_client_address(),
            revision=FirmwareRevision(filename=fn),
            vendor=vendor,
            target=request.form.get("target", "private"),
            auto_delete=request.form.get("auto-delete", False),
        )
        up.revision.write(blob)
        with db.session.no_autoflush:  # pylint: disable=no-member
            fw = _upload_firmware_raw(up)
    except (FileNotSupported, PermissionError, FileExistsError, KeyError) as e:
        return _json_error(str(e))

    # success
    return _json_success(
        msg="firmware created",
        uri=url_for("firmware.route_show", firmware_id=fw.firmware_id),
        errcode=201,
    )


@bp_upload.post("/token")
@auth.login_required
@csrf.exempt
def route_token():
    """Upload a .cab file to the LVFS service with a username and token"""

    # just proxy
    return _robot_upload_sync()


@bp_upload.route("/", methods=["GET", "POST"])
@login_required
@csrf.exempt
def route_robot():
    """Upload a .cab file to the LVFS service from a robot user"""

    # old URL being used
    if request.method != "POST":
        return redirect(url_for("upload.route_firmware"))

    # check is robot
    if not g.user.check_acl("@robot"):
        return _json_error("Not a robot user, please try again")

    # just proxy
    return _robot_upload_sync()


@bp_upload.route("/firmware", methods=["GET", "POST"])
@login_required
def route_firmware():
    """Upload a .cab file to the LVFS service"""

    # only accept form data
    if request.method != "POST":
        if not hasattr(g, "user"):
            return redirect(url_for("main.route_index"))
        if not _user_can_upload(g.user):
            return redirect(url_for("agreements.route_show"))
        vendor_ids = [res.value for res in g.user.vendor.restrictions]
        affiliations = (
            db.session.query(VendorAffiliation)
            .filter(VendorAffiliation.vendor_id_odm == g.user.vendor_id)
            .all()
        )
        uploads = (
            db.session.query(Upload)
            .filter(Upload.user_id == g.user.user_id)
            .order_by(Upload.upload_id.desc())
            .limit(3)
            .all()
        )
        return render_template(
            "upload.html",
            category="firmware",
            vendor_ids=vendor_ids,
            affiliations=affiliations,
            uploads=uploads,
        )

    # uploading for a custom vendor
    if "vendor_id" in request.form:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == int(request.form["vendor_id"]))
            .first()
        )
        if not vendor:
            flash("Specified vendor ID not found", "warning")
            return redirect(url_for("upload.route_firmware"))
    else:
        vendor = g.user.vendor

    # create
    try:
        blob = request.files["file"].read()
        fn = "{}-{}".format(
            hashlib.sha256(blob).hexdigest(),
            _get_sanitized_basename(os.path.basename(request.files["file"].filename)),
        )
    except KeyError as e:
        flash("Failed to upload: {}".format(str(e)), "warning")
        return redirect(url_for("upload.route_firmware"))
    if os.path.exists(fn):
        flash("The same filename {} already exists".format(fn), "warning")
        return redirect(url_for("upload.route_firmware"))

    # continue with form data
    try:
        up = Upload(
            user=g.user,
            addr=_get_client_address(),
            revision=FirmwareRevision(filename=fn),
            vendor=vendor,
            target=request.form.get("target", "private"),
            auto_delete=request.form.get("auto-delete", False),
        )
        up.revision.write(blob)
        with db.session.no_autoflush:  # pylint: disable=no-member
            fw = _upload_firmware_raw(up, sign=False)
    except (FileNotSupported, PermissionError, FileExistsError, KeyError) as e:
        flash("Failed to upload file: {}".format(str(e)), "warning")
        return redirect(url_for("upload.route_firmware"))

    # asynchronously sign
    _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")

    # success
    flash("Uploaded file {}".format(fw.revisions[0].filename), "info")
    return redirect(url_for("firmware.route_show", firmware_id=fw.firmware_id))


@bp_upload.post("/firmware/async")
@login_required
def route_firmware_async():
    """Upload a .cab file to the LVFS service"""

    # uploading for a custom vendor
    if "vendor_id" in request.form:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == int(request.form["vendor_id"]))
            .first()
        )
        if not vendor:
            flash("Specified vendor ID not found", "warning")
            return redirect(url_for("upload.route_firmware"))
    else:
        vendor = g.user.vendor

    # create
    try:
        blob = request.files["file"].read()
        fn = "{}-{}".format(
            hashlib.sha256(blob).hexdigest(),
            _get_sanitized_basename(os.path.basename(request.files["file"].filename)),
        )
    except KeyError as e:
        flash("Failed to upload: {}".format(str(e)), "warning")
        return redirect(url_for("upload.route_firmware"))
    if os.path.exists(fn):
        flash("The same filename {} already exists".format(fn), "warning")
        return redirect(url_for("upload.route_firmware"))

    # re-use existing upload revision
    rev = (
        db.session.query(FirmwareRevision)
        .filter(FirmwareRevision.filename == fn)
        .first()
    )
    if not rev:
        rev = FirmwareRevision(filename=fn)
    up = Upload(
        user=g.user,
        addr=_get_client_address(),
        revision=rev,
        vendor=vendor,
        target=request.form.get("target", "private"),
        auto_delete=request.form.get("auto-delete", False),
        status="Waiting for runner…",
    )

    # save to EFS and database
    up.revision.write(blob)
    db.session.add(up)
    db.session.commit()

    # asynchronously parse
    _async_upload_firmware.apply_async(args=(up.upload_id,), queue="firmware")

    flash("Starting upload for {}".format(up.upload_id), "info")
    return redirect(url_for("upload.route_firmware"))
